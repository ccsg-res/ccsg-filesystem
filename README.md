# CCSG File System

This library offers functions for accessing files, via FatFS, on a MicroSD card with either an SDIO interface or a SPI interface used by the CCSG EnviSense family of boards.

