#include "CCSG_SDCard.h"
#include "CCSG_SDHSMCI.h"


// **FIXME** replace with CCSG_Debug functions

#if defined(SD_DEBUG)
void Debug(const char *header, const char *msg)
{
	if (Serial)
	{
		Serial.print(header);
		Serial.print(": ");
		Serial.print(msg);
	}
}


void Debug(const char *header, unsigned char msg)
{
	if (Serial)
	{
		Serial.print(header);
		Serial.print(": ");
		Serial.print(msg);
	}
}


void Debug(unsigned char msg)
{
	if (Serial)
	{
		Serial.print(msg);
	}
}


void Debug(const char *msg)
{
	if (Serial)
	{
		Serial.print(msg);
	}
}
#else
void Debug(const char *header, const char *msg){}
void Debug(const char *header, unsigned char msg){}
void Debug(unsigned char msg){}
void Debug(const char *msg){}
#endif


bool StringEquals(const char *s1, const char *s2)
{
	int i = 0;
	while (s1[i] && s2[i])
	{
		if (tolower(s1[i]) != tolower(s2[i]))
		{
			return false;
		}
		i++;
	}

	return !(s1[i] || s2[i]);
}


MassStorage::MassStorage() :
		combinedName(combinedNameBuff, ARRAY_SIZE(combinedNameBuff))
{
	memset(&fileSystem, 0, sizeof(FATFS));
	findDir = new DIR();
}

void MassStorage::Init()
{
	sd_mmc_init();
	delay(20);

	bool abort = false;
	sd_mmc_err_t err;
	unsigned int now = millis();
	do
	{
		err = sd_mmc_check(0);
		if (err > SD_MMC_ERR_NO_CARD)
		{
			abort = true;
			delay(3000);// Wait a few seconds, so users have a chance to see the following error message
		}
		else
		{
			abort = (err == SD_MMC_ERR_NO_CARD && (millis() - now) > 5000);
		}

		if (abort)
		{
			Debug("MS", "Cannot initialize the SD card: ");

			switch (err)
			{
			case SD_MMC_ERR_NO_CARD:
				Debug("MS", "Card not found\n");
				break;
			case SD_MMC_ERR_UNUSABLE:
				Debug("MS", "Card is unusable, try another one\n");
				break;
			case SD_MMC_ERR_SLOT:
				Debug("MS", "Slot unknown\n");
				break;
			case SD_MMC_ERR_COMM:
				Debug("MS", "General communication error\n");
				break;
			case SD_MMC_ERR_PARAM:
				Debug("MS", "Illegal input parameter\n");
				break;
			case SD_MMC_ERR_WP:
				Debug("MS", "Card write protected\n");
				break;
			default:
				Debug("MS", "Unknown (code ");
				Debug("MS", err);
				Debug("MS", ")\n");
				break;
			}
			return;
		}
	} while (err != SD_MMC_OK);

	// Print some card details (optional)

	Debug("MS", "SD card detected!\nCapacity: ");
	Debug(sd_mmc_get_capacity(0));
	Debug("\n");
	Debug("MS", "Bus clock: ");
	Debug(sd_mmc_get_bus_clock(0));
	Debug("\n");
	Debug("MS", "Bus width: ");
	Debug(sd_mmc_get_bus_width(0));
	Debug("\n");
	Debug("MS", "Card type: ");
	switch (sd_mmc_get_type(0))
	{
	case CARD_TYPE_SD | CARD_TYPE_HC:
		Debug("SDHC\n");
		break;
	case CARD_TYPE_SD:
		Debug("SD\n");
		break;
	case CARD_TYPE_MMC | CARD_TYPE_HC:
		Debug("MMC High Density\n");
		break;
	case CARD_TYPE_MMC:
		Debug("MMC\n");
		break;
	case CARD_TYPE_SDIO:
		Debug("SDIO\n");
		return;
	case CARD_TYPE_SD_COMBO:
		Debug("SD COMBO\n");
		break;
	case CARD_TYPE_UNKNOWN:
	default:
		Debug("Unknown\n");
		return;
	}
}


const char* MassStorage::CombineName(const char *directory,
		const char *fileName)
{
	uint32_t out = 0;
	uint32_t in = 0;

	if (directory != NULL)
	{
		while (directory[in] != 0 && directory[in] != '\n')
		{
			combinedName[out] = directory[in];
			in++;
			out++;
			if (out >= combinedName.Length())
			{
				Debug("MS", "CombineName() buffer overflow.");
				out = 0;
			}
		}
	}

	if (in > 0 && directory[in - 1] != '/' && out < STRING_LENGTH - 1)
	{
		combinedName[out] = '/';
		out++;
	}

	in = 0;
	while (fileName[in] != 0 && fileName[in] != '\n')
	{
		combinedName[out] = fileName[in];
		in++;
		out++;
		if (out >= combinedName.Length())
		{
			Debug("MS", "CombineName() buffer overflow.");
			out = 0;
		}
	}
	combinedName[out] = 0;

	return combinedName.Pointer();
}


MassStorage SD_CARD;
