// ***************************************************************************************************
//
// PowerDueFirmware - SD Mass storage
//
// Radical simplification and reverting to FatFS
// 2019-0731 Bob Iannucci

#ifndef __CCSG_SDCARD_H__
#define __CCSG_SDCARD_H__

//#define SD_DEBUG

// Language-specific includes
#include <cctype>
#include <cstring>
#include <malloc.h>
#include <cstdlib>
#include <climits>

// Platform-specific includes
#include "Arduino.h"
#include "CCSG_SDConfiguration.h"

#include "CCSG_SDHSMCI.h"

// Macro to give us the number of elements in an array
#define ARRAY_SIZE(_x)	(sizeof(_x)/sizeof(_x[0]))
// Macro to give us the highest valid index into an array i.e. one less than the size
#define ARRAY_UPB(_x)	(ARRAY_SIZE(_x) - 1)

// Macro to assign an array from an initializer list
#if __cplusplus >= 201103L
// This version relies on C++'11 features (add '-std=gnu++11' to your CPP compiler flags)
#define ARRAY_INIT(_dest, _init) {static const decltype(_dest) _temp = _init; memcpy(_dest, _temp, sizeof(_dest)); }
#else
// This version relies on a gcc extension that is available only in older compilers
#define ARRAY_INIT(_dest, _init) _dest = _init
#define nullptr		(0)
#endif

// File handling
#define FILE_WRITE true
#define FILE_READ false
#define MAX_FILES (10)
#define FILE_BUF_LEN (256)
#define WEB_DIR "0:/www/" 						// Place to find web files on the SD card
#define SYS_DIR "0:/sys/" 						// Ditto - system files
#define TEMP_DIR "0:/tmp/" 						// Ditto - temporary files


enum IOStatus
{
	nothing = 0,
	byteAvailable = 1,
	atEoF = 2,
	clientLive = 4,
	clientConnected = 8
};


// Class to describe a string buffer, including its length. This saves passing buffer lengths around everywhere.
class StringRef
{
	char *p;		// pointer to the storage
	size_t len;		// number of characters in the storage

public:
	StringRef(char *pp, size_t pl) :
			p(pp), len(pl)
	{
	}

	size_t Length() const
	{
		return len;
	}
	size_t strlen() const;
	char* Pointer()
	{
		return p;
	}
	const char* Pointer() const
	{
		return p;
	}

	char& operator[](size_t index)
	{
		return p[index];
	}
	char operator[](size_t index) const
	{
		return p[index];
	}

	void Clear()
	{
		p[0] = 0;
	}

	int printf(const char *fmt, ...);
	int vprintf(const char *fmt, va_list vargs);
	int catf(const char *fmt, ...);
	size_t copy(const char *src);
	size_t cat(const char *src);
};


class MassStorage
{
public:
	const char* CombineName(const char *directory, const char *fileName);
	MassStorage(void);	//MassStorage(Platform* p);
	void Init();

private:
	FATFS fileSystem;
	DIR *findDir;
	char combinedNameBuff[FILENAME_LENGTH];
	StringRef combinedName;
};

extern MassStorage SD_CARD;

#endif
