/****************************************************************************************************

PowerDueFirmware - SD Configuration

This firmware is a port from:
"Version 0.1
18 November 2012
Adrian Bowyer
RepRap Professional Ltd
http://reprappro.com
Licence: GPL"

to work with the PowerDue
-----------------------------------------------------------------------------------------------------
****************************************************************************************************/

#ifndef __CCSG_SD_CONFIG_H__
#define __CCSG_SD_CONFIG_H__

#define NAME "PowerDue firmware"
#define VERSION "1.0.11"
#define DATE "2019-05-21"
#define AUTHORS "bob @ sv.cmu.edu"


// String lengths

#define STRING_LENGTH 1024
#define SHORT_STRING_LENGTH 40

#define FILENAME_LENGTH 256
#define GCODE_REPLY_LENGTH 2048

#define LIST_SEPARATOR ':'						// Lists in G Codes
#define FILE_LIST_SEPARATOR ','					// Put this between file names when listing them
#define FILE_LIST_BRACKET '"'					// Put these round file names when listing them

#define LONG_TIME 300.0 // Seconds

#define EOF_STRING "<!-- **EoF** -->"           // For HTML uploads
#endif
