 /*******************************************************************************
 * File system tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#include "CCSG_FileSystem.h"
#include "CCSG_Debug.h"


#if defined(BOARD_ENVISENSE_FAMILY)

#define WRITE_ENABLE true

FATFS FILE_SYSTEM;


// DO NOT USE DEBUG STATEMENTS IN THIS FUNCTION:
// The debug logging code opens the log for appending.
// You could create a nasty loop.
FRESULT ccsg_filesystem_open_append(FIL *file, const char *path)
{
	FRESULT file_result;
	int retryCount = 5;

	while (retryCount > 0)
	{
		file_result = f_open(file, path, FA_WRITE | FA_OPEN_ALWAYS);
		if (file_result == FR_OK)
		{
			// Seek to end of the file to append
			file_result = f_lseek(file, f_size(file));
			if (file_result != FR_OK)
			{
				DEBUG_SERIAL.print(F("[filesys ] f_lseek failed on file size of "));
				DEBUG_SERIAL.println(f_size(file));
				f_close(file);
				retryCount--;
//				ccsg_debug_morse_led((char*)"E");
//				delay(100);  // milliseconds between retry attempts
			}
			else
				return file_result;  // worked!
		}
		else
		{
			retryCount--;
			DEBUG_SERIAL.print(F("[filesys ] f_open failed with code "));
			DEBUG_SERIAL.println(file_result);
			ccsg_debug_morse_led((char*)"OA");
			delay(100);  // milliseconds between retry attempts
		}
	}
	return file_result;
}


// Call ccsg_filesystem_init() before invoking this function
void ccsg_filesystem_test()
{
	FRESULT file_result;
	FIL file;

	const UINT buffer_size = 100;
	char buffer[buffer_size];
	UINT n_bytes_read;

	CCSG_DEBUG_PRINTLN(F("[filesys ] *** File System Tests"));
	bool sd_card_detected_p = !digitalRead(SD_DETECT);

	if (!sd_card_detected_p)
		CCSG_DEBUG_PRINTLN(F("[filesys ] No SD card detected"));
	else
	{
		CCSG_DEBUG_PRINTLN(F("[filesys ] SD card detected"));
		file_result = f_mkdir(TEST_DIRECTORY);
		if (file_result != FR_OK)
			CCSG_DEBUG_PRINTF("[filesys ] Error creating test directory: %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
		else
		{
			CCSG_DEBUG_PRINTLN(F("[filesys ] Test directory created"));
			file_result = f_unlink(TEST_DIRECTORY);
			if (file_result == FR_OK)
				CCSG_DEBUG_PRINTLN(F("[filesys ] Deleted directory successfully"));
			else
				CCSG_DEBUG_PRINTF("[filesys ] Failed to delete directory: %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
		}

		// Assumes the SD card is powered on and the file system is mounted
		file_result = ccsg_filesystem_open_append(&file, TEST_FILE_NAME);
		if (file_result != FR_OK)
			CCSG_DEBUG_PRINTF("[filesys ] Error opening test file: %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
		else
		{
			f_printf(&file, "Line 1\r\n");
			f_close(&file);
			file_result = ccsg_filesystem_open_append(&file, TEST_FILE_NAME);
			if (file_result != FR_OK)
				CCSG_DEBUG_PRINTF("[filesys ] Error opening for append (second time): %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
			else
			{
				f_printf(&file, "Line 2\r\n");
				f_close(&file);
				file_result = f_open(&file, TEST_FILE_NAME, FA_READ);
				if (file_result != FR_OK)
					CCSG_DEBUG_PRINTF("[filesys ] Error opening for reading: %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
				else
				{
					CCSG_DEBUG_PRINTLN(F("[filesys ] Contents of the file:\r\n"));
					while ((f_read(&file, &buffer, buffer_size, &n_bytes_read)==FR_OK) && (n_bytes_read>0))
						for (uint32_t i=0; i<n_bytes_read; i++)
							CCSG_DEBUG_PRINTF("%c", buffer[i]);
				}
				CCSG_DEBUG_PRINTLN(F(""));
				f_close(&file);
				file_result = f_unlink(TEST_FILE_NAME);
				if (file_result == FR_OK)
					CCSG_DEBUG_PRINTLN(F("[filesys ] Deleted file successfully"));
				else
					CCSG_DEBUG_PRINTF("[filesys ] Failed to delete file: %s\r\n", ccsg_filesystem_string_for_fatfs_error(file_result));
			}
		}
	}

	CCSG_DEBUG_PRINTLN(F("[filesys ] *** End File System Tests"));
}


// This function is called as part of the debug logging system.  As such, don't
// invoke any debug_ functions that would cause a recursion!!
FRESULT ccsg_filesystem(gpio_action_t action)
{
	static int enableStackDepth = 0;
	FRESULT file_result;
	FRESULT enableFileResult = FR_OK;

	switch (action)
	{
	case ACTION_RESET:
		ccsg_power_set_state(DEVICE_SD_CARD, ACTION_RESET);
		return FR_OK;
		break;
	case ACTION_ENABLE:
		if (enableStackDepth == 0)
		{
			ccsg_power_set_state(DEVICE_SD_CARD, ACTION_ENABLE);
			delay(10);
			SD_CARD.Init();
			// Create the file system we will use throughout
			enableFileResult = f_mount(&FILE_SYSTEM, "", 0);
			if (enableFileResult != FR_OK)
				ccsg_debug_morse_led((char*)"F0");
		}
		enableStackDepth++;
		return enableFileResult;
		break;
	case ACTION_DISABLE:
		enableStackDepth--;
		if (enableStackDepth == 0)
		{
			// Unmount the file system
			file_result = f_mount(NULL, "", 0);
			if (file_result != FR_OK)
				ccsg_debug_morse_led((char*)"F1");
			delay(10);  // Allow any pending actions to complete
			ccsg_power_set_state(DEVICE_SD_CARD, ACTION_DISABLE);
			return file_result;
		}
		else
			return enableFileResult;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		if (enableStackDepth != 0)
		{
			// Unmount the file system
			file_result = f_mount(NULL, "", 0);
			if (file_result != FR_OK)
				ccsg_debug_morse_led((char*)"F2");
			delay(10);  // Allow any pending actions to complete
//			f_mount(NULL, "", 0);  // deleted - new
//			ccsg_power_sd_card(ACTION_PREPARE_TO_SLEEP); // this gets done separately in _ccsg_deep_sleep()
			return file_result;
		}
		else
		{
			ccsg_power_set_state(DEVICE_SD_CARD, ACTION_PREPARE_TO_SLEEP);
			return FR_OK;
		}
		break;
	case ACTION_WAKE_FROM_SLEEP:
//		ccsg_power_sd_card(ACTION_WAKE_FROM_SLEEP);  // this gets done separately in _ccsg_deep_sleep()
		if (enableStackDepth > 0)
		{
			delay(10);       // new
			SD_CARD.Init();  // new
			enableFileResult = f_mount(&FILE_SYSTEM, "", 0);
			if (enableFileResult != FR_OK)
				ccsg_debug_morse_led((char*)"F3");
			return enableFileResult;
		}
		else
			return FR_OK;
		break;
	default:
		return FR_INVALID_PARAMETER;
		break;
	}
}

#else

FRESULT ccsg_filesystem_test()
{
	return FR_INVALID_PARAMETER;

}


FRESULT ccsg_filesystem(gpio_action_t action)
{
	return FR_INVALID_PARAMETER;
}


FRESULT ccsg_filesystem_save_schedule_to_sd(char* array, int size)
{
	return FR_INVALID_PARAMETER;
}


FRESULT ccsg_filesystem_restore_schedule_from_sd(char* array, int size)
{
	return FR_INVALID_PARAMETER;
}
#endif


char* ccsg_filesystem_string_for_fatfs_error(FRESULT err)
{
	switch (err)
	{
	case FR_OK:
		return (char*)"FR_OK";
	case FR_DISK_ERR:				/* (1) A hard error occurred in the low level disk I/O layer */
		return (char*)"FR_DISK_ERR";
	case FR_INT_ERR:				/* (2) Assertion failed */
		return (char*)"FR_INT_ERR";
	case FR_NOT_READY:				/* (3) The physical drive cannot work */
		return (char*)"FR_NOT_READY";
	case FR_NO_FILE:				/* (4) Could not find the file */
		return (char*)"FR_NO_FILE";
	case FR_NO_PATH:				/* (5) Could not find the path */
		return (char*)"FR_NO_PATH";
	case FR_INVALID_NAME:			/* (6) The path name format is invalid */
		return (char*)"FR_INVALID_NAME";
	case FR_DENIED:					/* (7) Access denied due to prohibited access or directory full */
		return (char*)"FR_DENIED";
	case FR_EXIST:					/* (8) Access denied due to prohibited access */
		return (char*)"FR_EXIST";
	case FR_INVALID_OBJECT:			/* (9) The file/directory object is invalid */
		return (char*)"FR_INVALID_OBJECT";
	case FR_WRITE_PROTECTED:		/* (10) The physical drive is write protected */
		return (char*)"FR_WRITE_PROTECTED";
	case FR_INVALID_DRIVE:			/* (11) The logical drive number is invalid */
		return (char*)"FR_INVALID_DRIVE";
	case FR_NOT_ENABLED:			/* (12) The volume has no work area */
		return (char*)"FR_NOT_ENABLED";
	case FR_NO_FILESYSTEM:			/* (13) There is no valid FAT volume */
		return (char*)"FR_NO_FILESYSTEM";
	case FR_MKFS_ABORTED:			/* (14) The f_mkfs() aborted due to any problem */
		return (char*)"FR_MKFS_ABORTED";
	case FR_TIMEOUT:				/* (15) Could not get a grant to access the volume within defined period */
		return (char*)"FR_TIMEOUT";
	case FR_LOCKED:					/* (16) The operation is rejected according to the file sharing policy */
		return (char*)"return";
	case FR_NOT_ENOUGH_CORE:		/* (17) LFN working buffer could not be allocated */
		return (char*)"FR_NOT_ENOUGH_CORE";
	case FR_TOO_MANY_OPEN_FILES:	/* (18) Number of open files > FF_FS_LOCK */
		return (char*)"FR_TOO_MANY_OPEN_FILES";
	case FR_INVALID_PARAMETER:		/* (19) Given parameter is invalid */
		return (char*)"FR_INVALID_PARAMETER";
	default:
		return (char*)"UNKNOWN";
	}
}



