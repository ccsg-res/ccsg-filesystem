/*******************************************************************************
 * File system tools for EnviSense and PowerDue boards
 *
 * Copyright (c) 2019 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_FILESYSTEM_H__
#define __CCSG_FILESYSTEM_H__

#include "variant.h"
#include "CCSG_BoardConfig.h"
#include "CCSG_SDCard.h"
#include "CCSG_Power.h"

void ccsg_filesystem_test();
FRESULT ccsg_filesystem(gpio_action_t action);
FRESULT ccsg_filesystem_open_append(FIL *file, const char *path);
char* ccsg_filesystem_string_for_fatfs_error(FRESULT err);

#endif  // __CCSG_FILESYSTEM_H__
